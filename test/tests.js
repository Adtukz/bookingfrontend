const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../server');
const expect = chai.expect;
const should = chai.should();
chai.use(chaiHttp);

//test home page exists
describe("GET /", () => {
  it("Home Page", (done) => {
       chai.request(app)
           .get('/')
           .end((err, res) => {
               expect(res).to.have.status(200);
               expect(res).to.be.html;
               res.text.should.contain('<h2>Booking form</h2>');
               done();
     });
   });
});

//check flight back exists
describe("GET /flightsearch", () => {
  it("Flight Search", (done) => {
       chai.request(app)
           .get('/flightsearch/')
           .end((err, res) => {
               expect(res).to.have.status(200);
               expect(res).to.be.html;
               res.text.should.contain('');
               done();
     });
   });
});
