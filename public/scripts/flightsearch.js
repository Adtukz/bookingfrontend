//variables to hold url, the array of flights returned and the triptype choosen
let url = 'http://40.113.20.233:3000/api/';
let flightListOne = {};
let flightListTwo = {};
let tripTypeValue;

//when document is ready we want to set the page up with the data from previous page
$(document).ready(function() {
  let searchParams = new URLSearchParams(window.location.search);

  let tripType = searchParams.get('tripType');
  let locationOut = searchParams.get('locationOut');
  let locationBack = searchParams.get('locationBack');
  let passengers = searchParams.get('passengers');
  $('#locationOutLabel').val(locationOut);
  $('#locationBackLabel').val(locationBack);
  $('#passengersLabel').val(passengers);

  //depending on trip type the div will represent the previous page
  if(tripType === "return") {
    let dateOut = searchParams.get('dateOut');
    let dateBack = searchParams.get('dateBack');
    $('#bookingType').html("Your Return Booking Details");
    $('#dateOutLabel').val(dateOut);
    $('#dateBackLabel').val(dateBack);
    populateTableOne();
    tripTypeValue = 1;
  } else if (tripType === "oneway") {
    $('#dateOutDiv').attr("class", "col-md-8");
    $('#dateBackDiv').attr("class", "d-none");
    let dateOut = searchParams.get('dateOut');
    $('#dateOutLabel').val(dateOut);
    $('#bookingType').html("Your One Way Booking Details");
    tripTypeValue = 0;
    populateTableOne();
    hideTableTwo();
  }

});

//get function to fill the table with data from the API
function populateTableOne() {

  let leavingdate = new Date($('#dateOutLabel').val());
  let fromairport = $('#locationOutLabel').val();
  let toairport = $('#locationBackLabel').val();
  let passengers = $('#passengersLabel').val();

  $.ajax({
    type: "GET",
    url: url + `flights/getflights/${leavingdate}/${fromairport}/${toairport}/${passengers}`,
    contentType: "application/json",
    dataType: 'json',
    cache: false,
    success: function(data){
      console.log(data);
        flightListOne = data;
        let bookings = [];
        for(let i = 0; i < data.length; i++) {

          let dateOut = new Date(data[i].Depart_date);
          let dateOutHours = dateOut.getHours();
          let dateOutMinutes = dateOut.getMinutes();
          let dateOutMMDDYYYY = `${dateOut.getMonth() + 1}/${dateOut.getDate()}/${dateOut.getFullYear()}`

          let dateBack = new Date(data[i].Arrive_date);
          let dateBackHours = dateBack.getHours();
          let dateBackMinutes = dateBack.getMinutes();
          let dateBackMMDDYYYY = `${dateBack.getMonth() + 1}/${dateBack.getDate()}/${dateBack.getFullYear()}`

          bookings +=
            `
              <tr class="tableBooking">
                <td> ${data[i].Flight_ID} </td>
                <td> ${dateOutMMDDYYYY} </td>
                <td> ${dateOutHours<10?'0'+dateOutHours:dateOutHours}:${dateOutMinutes<10?'0'+dateOutMinutes:dateOutMinutes} </td>
                <td> ${dateBackMMDDYYYY} </td>
                <td> ${dateBackHours<10?'0'+dateBackHours:dateBackHours}:${dateBackMinutes<10?'0'+dateBackMinutes:dateBackMinutes} </td>
                <td> ${Math.round(data[i].Price * 100) / 100} </td>
                <td> <input type=button class='btn btn-primary btn-block selectFlightOneButton' value=Select id=${data[i].Flight_ID}> </td>
              </tr>
            `
        }
        $('#bookingTableFlightOne tbody').html(bookings);
        if(tripTypeValue === 1) {
          populateTableTwo();
        }
    }
  })
}

function populateTableTwo() {

  let leavingdate = new Date($('#dateBackLabel').val());
  let fromairport = $('#locationBackLabel').val();
  let toairport = $('#locationOutLabel').val();
  let passengers = $('#passengersLabel').val();

  $.ajax({
    type: "GET",
    url: url + `flights/getflights/${leavingdate}/${fromairport}/${toairport}/${passengers}`,
    contentType: "application/json",
    dataType: 'json',
    cache: false,
    success: function(data){
      console.log(data);
        flightListTwo = data;
        let bookings = []
        for(let i = 0; i < data.length; i++) {

          let dateOut = new Date(data[i].Depart_date);
          let dateOutHours = dateOut.getHours();
          let dateOutMinutes = dateOut.getMinutes();
          let dateOutMMDDYYYY = `${dateOut.getMonth() + 1}/${dateOut.getDate()}/${dateOut.getFullYear()}`

          let dateBack = new Date(data[i].Arrive_date);
          let dateBackHours = dateBack.getHours();
          let dateBackMinutes = dateBack.getMinutes();
          let dateBackMMDDYYYY = `${dateBack.getMonth() + 1}/${dateBack.getDate()}/${dateBack.getFullYear()}`

          bookings +=
            `
              <tr class="tableBooking">
                <td> ${data[i].Flight_ID} </td>
                <td> ${dateOutMMDDYYYY} </td>
                <td> ${dateOutHours<10?'0'+dateOutHours:dateOutHours}:${dateOutMinutes<10?'0'+dateOutMinutes:dateOutMinutes} </td>
                <td> ${dateBackMMDDYYYY} </td>
                <td> ${dateBackHours<10?'0'+dateBackHours:dateBackHours}:${dateBackMinutes<10?'0'+dateBackMinutes:dateBackMinutes} </td>
                <td> ${Math.round(data[i].Price * 100) / 100} </td>
                <td> <input type=button class='btn btn-primary btn-block selectFlightTwoButton' value=Select id=${data[i].Flight_ID}> </td>
              </tr>
            `
        }
        $('#bookingTableFlightTwo tbody').html(bookings);
    }
  })
}

//if oneway flight we hide second table from view
function hideTableTwo() {
  $('#returnFlightTable').attr("class","d-none");
}

//if return is clicked we redirect to previous page
$('#returnButton').click(function(event) {
  event.preventDefault();
  window.location = "/";
});

//if select flight one button is clicked we use the id to search the array of flights
//when the flight is found we fill the fields with its data for the user to see and confirm
$(document).on('click', '.selectFlightOneButton' , function(event) {
  for(let i = 0; i < flightListOne.length; i++) {
    if(flightListOne[i].Flight_ID == event.currentTarget.id) {

      let dateOut = new Date(flightListOne[i].Depart_date);
      let dateOutHours = dateOut.getHours();
      let dateOutMinutes = dateOut.getMinutes();
      let dateOutMMDDYYYY = `${dateOut.getMonth() + 1}/${dateOut.getDate()}/${dateOut.getFullYear()}`

      let dateBack = new Date(flightListOne[i].Arrive_date);
      let dateBackHours = dateBack.getHours();
      let dateBackMinutes = dateBack.getMinutes();
      let dateBackMMDDYYYY = `${dateBack.getMonth() + 1}/${dateBack.getDate()}/${dateBack.getFullYear()}`

      $('#flightOneTitle').html("Selected Departing Flight");

      $('#flightOneDateOut').val(`${dateOutMMDDYYYY}`);
      $('#flightOneDateLand').val(`${dateBackMMDDYYYY}`);
      $('#flightOneTimeOut').val(`${dateOutHours<10?'0'+dateOutHours:dateOutHours}:${dateOutMinutes<10?'0'+dateOutMinutes:dateOutMinutes}`);
      $('#flightOneTimeLand').val(`${dateBackHours<10?'0'+dateBackHours:dateBackHours}:${dateBackMinutes<10?'0'+dateBackMinutes:dateBackMinutes}`);
      $('#flightOneCost').val(Math.round(flightListOne[i].Price*100)/100);
      $('#flightOneId').val(flightListOne[i].Flight_ID);
      $('#flightOneDateDepart').val(flightListOne[i].Depart_date);

      $('#bookingTableFlightOne').attr("class", "d-none");
      $('#flightOneFields').attr("class", "row");

      $('#resetFlightOne').attr("class", "btn btn-warning btn-lg btn-block");

    }
  }
  //if a return flight is choosen we want to show the confirm flights button
  // if not we dont want it to show yet
  //if the user has selected one way flight we want the onfirm button to appear
  if(tripTypeValue === 1) {
    if($('#flightTwoId').val()) {
      $('#continueWithFlights').attr("class", "col-md-12");
    } else {
      $('#continueWithFlights').attr("class", "d-none");
    }
  } else {
    $('#continueWithFlights').attr("class", "col-md-12");
    $('#submitButton').val("Continue With Selected Flight");
  }
});

//similarly if the second flight is picked we want the fields to populate
//and the button to show if the first flight was choosen already
$(document).on('click', '.selectFlightTwoButton' , function(event) {
  for(let i = 0; i < flightListTwo.length; i++) {
    if(flightListTwo[i].Flight_ID == event.currentTarget.id) {

      let dateOut = new Date(flightListTwo[i].Depart_date);
      let dateOutHours = dateOut.getHours();
      let dateOutMinutes = dateOut.getMinutes();
      let dateOutMMDDYYYY = `${dateOut.getMonth() + 1}/${dateOut.getDate()}/${dateOut.getFullYear()}`

      let dateBack = new Date(flightListTwo[i].Arrive_date);
      let dateBackHours = dateBack.getHours();
      let dateBackMinutes = dateBack.getMinutes();
      let dateBackMMDDYYYY = `${dateBack.getMonth() + 1}/${dateBack.getDate()}/${dateBack.getFullYear()}`

      $('#flightTwoTitle').html("Selected Return Flight");

      $('#flightTwoDateOut').val(`${dateOutMMDDYYYY}`);
      $('#flightTwoDateLand').val(`${dateBackMMDDYYYY}`);
      $('#flightTwoTimeOut').val(`${dateOutHours<10?'0'+dateOutHours:dateOutHours}:${dateOutMinutes<10?'0'+dateOutMinutes:dateOutMinutes}`);
      $('#flightTwoTimeLand').val(`${dateBackHours<10?'0'+dateBackHours:dateBackHours}:${dateBackMinutes<10?'0'+dateBackMinutes:dateBackMinutes}`);
      $('#flightTwoCost').val(Math.round(flightListTwo[i].Price*100)/100);
      $('#flightTwoId').val(flightListTwo[i].Flight_ID);
      $('#flightTwoDateDepart').val(flightListTwo[i].Depart_date);

      $('#bookingTableFlightTwo').attr("class", "d-none");
      $('#flightTwoFields').attr("class", "row");

      $('#resetFlightTwo').attr("class", "btn btn-warning btn-lg btn-block");

    }
  }

  if($('#flightOneId').val()) {
    $('#continueWithFlights').attr("class", "col-md-12");
  } else {
    $('#continueWithFlights').attr("class", "d-none");
  }
});

//if reset is clicked we empty the fields and reshow the table
$('#resetFlightOne').click( function(event) {

  $('#flightOneTitle').html("Please Choose Departing Flight");

  $('#flightOneDateOut').val("");
  $('#flightOneDateLand').val("");
  $('#flightOneCost').val("");
  $('#flightOneId').val("");
  $('#flightOneDateDepart').val("");

  $('#bookingTableFlightOne').attr("class", "table table-striped table-hover tableBooking");
  $('#flightOneFields').attr("class", "row d-none");
  $('#resetFlightOne').attr("class", "d-none");
});

//if reset is clicked we empty the fields and reshow the table
$('#resetFlightTwo').click( function(event) {

  $('#flightTwoTitle').html("Please Choose Return Flight");

  $('#flightTwoDateOut').val("");
  $('#flightTwoDateLand').val("");
  $('#flightTwoCost').val("");
  $('#flightTwoId').val("");
  $('#flightOneDateDepart').val("");

  $('#bookingTableFlightTwo').attr("class", "table table-striped table-hover tableBooking");
  $('#flightTwoFields').attr("class", "row d-none");
  $('#resetFlightTwo').attr("class", "d-none");
});

//if the flights are correct and the user submits the flights they are moved to next page
$('#submitButton').click(function(event) {

  let flightsearchUrl = "";
  let flightOutId, flightBackId, dateOut, dateBack;

  if(tripTypeValue === 1) {
    flightsearchUrl = "/flightconfirm/?tripType=return";
    flightOutId = $('#flightOneId').val();
    flightBackId = $('#flightTwoId').val();
    dateOut = $('#flightOneDateDepart').val();
    dateBack = $('#flightTwoDateDepart').val();
    flightsearchUrl += "&flightBackId=" + flightBackId;
    flightsearchUrl += "&dateBack=" + dateBack;
  } else {
    flightsearchUrl = "/flightconfirm/?tripType=oneway";
    flightOutId = $('#flightOneId').val();
    dateOut = $('#flightOneDateDepart').val();
  }

  let passengers = $('#passengersLabel').val();
  flightsearchUrl += "&flightOutId=" + flightOutId;
  flightsearchUrl += "&dateOut=" + dateOut;
  flightsearchUrl += "&passengers=" + passengers;

  window.location = flightsearchUrl;
});
