//commented url out to prevent accidently posting
//let url = 'http://localhost:5000/api/';

for (let i = 1; i <= 1000; i++) {

  let randomDay = Math.floor((Math.random()*31)+1);
  let randomMonth = Math.floor((Math.random()*13)+1);
  let randomYear = Math.floor((Math.random()*4)+2017);
  let randomTripLength = Math.floor((Math.random() * 15) + 1);

  let randUser = Math.floor((Math.random()*100)+1)
  let userId = -randUser;
  let passengers = Math.floor((Math.random()*13)+1);
  let dateOut = new Date(randomYear, randomMonth, randomDay);
  let dateBack = new Date(randomYear, randomMonth, randomDay);
  dateBack.setDate(dateBack.getDate() + randomTripLength);
  let flightOutId = Math.floor((Math.random()*9999)+1);
  let flightBackId = Math.floor((Math.random()*9999)+1);

  let onewayChance = Math.random();
  let newBooking = {
  };

  if(onewayChance < 0.05) {
    let cancelChance = Math.random();
    if(cancelChance < 0.05) {
      newBooking = {
        UserId: userId,
        Passengers: passengers,
        DateOut: dateOut,
        FlightOutId: flightOutId,
        Cancelled: 1,
        ReturnOneway: 0
      }
    } else {
      newBooking = {
        UserId: userId,
        Passengers: passengers,
        DateOut: dateOut,
        FlightOutId: flightOutId,
        Cancelled: 0,
        ReturnOneway: 0
      }
    }
  } else {
    let cancelChance = Math.random();
    if (cancelChance < 0.05) {
      newBooking = {
        UserId: userId,
        Passengers: passengers,
        DateOut: dateOut,
        DateBack: dateBack,
        FlightOutId: flightOutId,
        FlightBackId: flightBackId,
        Cancelled: 1,
        ReturnOneway: 1
      }
    } else {
      newBooking = {
        UserId: userId,
        Passengers: passengers,
        DateOut: dateOut,
        DateBack: dateBack,
        FlightOutId: flightOutId,
        FlightBackId: flightBackId,
        Cancelled: 0,
        ReturnOneway: 1
      }
    }
  }

  console.log(newBooking);

  //commented post out incase of accidently running script again
  // $.ajax({
  //   type: "POST",
  //   url: url,
  //   async: false,
  //   contentType: "application/json",
  //   data: JSON.stringify(newBooking),
  //   dataType: 'json',
  //   cache: false,
  //   success: function(data){
  //     console.log(data)
  //   }
  // })
}
