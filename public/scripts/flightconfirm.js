// variables for url and searchparams
let url = 'http://40.113.20.233:3000/api/';
let searchParams;
let confirmedFlightOneId;
let confirmedFlightTwoId;

//when document is ready the flight details will be filled out for the user
$(document).ready(function() {
  searchParams = new URLSearchParams(window.location.search);

  let tripType = searchParams.get('tripType');

  if(tripType === "return") {
    if(confirmFlightExists(searchParams.get('flightOutId'), searchParams.get('flightOutDate') && searchParams.get('flightBackId'), searchParams.get('flightBackDate'))) {
          fillFlightDetails(1);
    }
  } else if (tripType === "oneway") {
    if(confirmFlightExists(searchParams.get('flightOutId'), searchParams.get('flightOutDate') {
      fillFlightDetails(0);
      $("#flightTwoDetails").attr("class","d-none");
    }
  }
});

//fill details depends on if return or oneway trip
function fillFlightDetails(type) {
  //first flight will always been shown oneway or return flight
  $.ajax({
    type: "GET",
    url: url + `flights/${confirmedFlightOneId}`,
    contentType: "application/json",
    dataType: 'json',
    async: true,
    cache: false,
    success: function(data){
      console.log(data);

      let dateOut = new Date(data[0].Depart_date);
      let dateOutHours = dateOut.getHours();
      let dateOutMinutes = dateOut.getMinutes();
      let dateOutMMDDYYYY = `${dateOut.getMonth()}/${dateOut.getDate()}/${dateOut.getFullYear()}`;
      if(dateOutHours < 10){
        dateOutHours = '0' + dateOutHours
      }
      if(dateOutMinutes < 10){
        dateOutMinutes = '0' + dateOutMinutes
      }
      let departTime = `${dateOutHours}:${dateOutMinutes}`;

      let dateLand = new Date(data[0].Arrive_date);
      let dateLandHours = dateLand.getHours() + 15;
      let dateLandMinutes = dateLand.getMinutes() + 38;
      let dateLandMMDDYYYY = `${dateLand.getMonth()}/${dateLand.getDate()}/${dateLand.getFullYear()}`;
      if(dateLandHours < 10){
        dateLandHours = '0' + dateLandHours
      }
      if(dateLandMinutes < 10){
        dateLandMinutes = '0' + dateLandMinutes
      }
      let landTime = `${dateLandHours}:${dateLandMinutes}`;

      $('#flightOneDepartDate').val(dateOutMMDDYYYY);
      $('#flightOneDepartTime').val(departTime);
      $('#flightOneLandDate').val(dateLandMMDDYYYY);
      $('#flightOneLandTime').val(landTime);
      $('#flightOnePassengers').val(data[0].Passengers);
      $('#flightOneCost').val(data[0].Price);
    }
  })
  // if return was selected the second flight will appear
  if(type === 1) {
    $.ajax({
      type: "GET",
      url: url + `flights/${confirmedFlightTwoId}`,
      contentType: "application/json",
      dataType: 'json',
      async: true,
      cache: false,
      success: function(data){
        console.log(data);

        let dateOut = new Date(data[0].Depart_date);
        let dateOutHours = dateOut.getHours();
        let dateOutMinutes = dateOut.getMinutes();
        let dateOutMMDDYYYY = `${dateOut.getMonth()}/${dateOut.getDate()}/${dateOut.getFullYear()}`;
        if(dateOutHours < 10){
          dateOutHours = '0' + dateOutHours
        }
        if(dateOutMinutes < 10){
          dateOutMinutes = '0' + dateOutMinutes
        }
        let departTime = `${dateOutHours}:${dateOutMinutes}`;

        let dateLand = new Date(data[0].Arrive_date);
        let dateLandHours = dateLand.getHours() + 15;
        let dateLandMinutes = dateLand.getMinutes() + 38;
        let dateLandMMDDYYYY = `${dateLand.getMonth()}/${dateLand.getDate()}/${dateLand.getFullYear()}`;
        if(dateLandHours < 10){
          dateLandHours = '0' + dateLandHours
        }
        if(dateLandMinutes < 10){
          dateLandMinutes = '0' + dateLandMinutes
        }
        let landTime = `${dateLandHours}:${dateLandMinutes}`;

        $('#flightTwoDepartDate').val(dateOutMMDDYYYY);
        $('#flightTwoDepartTime').val(departTime);
        $('#flightTwoLandDate').val(dateLandMMDDYYYY);
        $('#flightTwoLandTime').val(landTime);
        $('#flightTwoPassengers').val(data[0].Passengers);
        $('#flightTwoCost').val(data[0].Price);
      }
    })
  }
}

confirmFlightExists(flightId, flightDepartDate) {
  $.ajax({
    type: "GET",
    url: url + `flights/flightconfirm/${flightId}/${flightDepartDate}`,
    contentType: "application/json",
    dataType: 'json',
    async: true,
    cache: false,
    success: function(data){
      console.log(data);
      return true;
    }
  })
  return false;
}


//if return is clicked we redirect to home page
$('#returnButton').click(function(event) {
  event.preventDefault();
  window.location = "/";
});

//if purchase is clicked the user will purchase the selected flights
$('#purchasButton').click(function(event) {
  event.preventDefault();
  console.log("Purchase!");
});
