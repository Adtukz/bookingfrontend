let airportUrl = 'http://40.113.20.233:3000/api/airports/searchareaflights/';
let airports = {};

//On document load create the date pickers
$( document ).ready(function() {
  createDatePickers();
  $('#modalDiv').addClass('d-none');
  let passengersDropDown = $("#passengers");
  for (i=1;i<=15;i++){
    passengersDropDown.append($('<option></option>').val(i).html(i));
  }
});

//when the user has selected the out going flight date,
//the second date picker has its minimum set to the choosen
//date and is opened up to show the user where to enter
//their return date
$("#flightOutDate").datepicker({
  onSelect: function(dateText) {
    $("#flightBackDate").datepicker("setDate", dateText);
    $("#flightBackDate").datepicker( "option", "minDate", dateText );
  },
  onClose: function() {
  $("#flightBackDate").datepicker("show");
  }
});

//if the user selects one way flight the return date
//div is hidden and the date out div is expanded to fill
//the space left behind
$('#bookingForm').click(function() {
  if($('#onewayFlight').is(':checked')) {
    $("#flightOutDateDiv").attr("class","col-md-8 mb-3 onewayFlight");
    $("#flightBackDateDiv").attr("class", "d-none");
  } else {
    $("#flightOutDateDiv").attr("class","col-md-4 mb-3");
    $("#flightBackDateDiv").attr("class", "col-md-4 mb-3");
  }
});

//when the user wants to reset the form we need to reset
//teh date pickers also and the validation of the submitForm
//the return date picker is not reset properly due to it
//reseting to the outgoing flight date chosen before the reset
$('#resetButton').click(function(event) {
  event.preventDefault();
  $('#bookingForm').trigger('reset');
  createDatePickers();
  $("#flightBackDate").datepicker().datepicker("setDate", new Date());
  $("#bookingForm").attr("class", "needs-validation");
});

//when the form is submitted we check validation is correct
//if it is we submit the form
$('#submitButton').click(function(event) {
  event.preventDefault();
  const form = $("#bookingForm");
  form.attr("class", "was-validated");
  if (form[0].checkValidity() === true) {
    submitForm();
  }
});


let showChatbot = false;
//when the form is submitted we check validation is correct
//if it is we submit the form
$('#botButton').click(function(event) {
  event.preventDefault();
  const modalDiv = $("#modalDiv");
  const formDiv = $("#formDiv");
  if(showChatbot === false) {
    formDiv.attr("class", "col-md-8");
    modalDiv.attr("class", "col-md-4");
    showChatbot = true;
  } else {
    formDiv.attr("class", "col-md-12")
    modalDiv.attr("class","d-none");
    showChatbot = false;
  }

});


//gathers all the data from the validated form
//adds it to a url and redirects to it
//redirected url is the flightsearch page
//with all of the data in the url`
//so that it can be pulled onto the next page
function submitForm() {
  let flightOut = $('#flightOutIATA').val();
  let flightBack = $('#flightBackIATA').val();
  let dateOut = $('#flightOutDate').val();
  let dateBack = $('#flightBackDate').val();
  let passengers = $('#passengers').val();

  let flightsearchUrl = "";

  if($('#onewayFlight').is(':checked')) {
    flightsearchUrl = "/flightsearch/?tripType=oneway";
  } else if($('#returnFlight').is(':checked')) {
    flightsearchUrl = "/flightsearch/?tripType=return";
  }

  flightsearchUrl += "&locationOut=" + flightOut;
  flightsearchUrl += "&locationBack=" + flightBack;
  flightsearchUrl += "&dateOut=" + dateOut;
  flightsearchUrl += "&dateBack=" + dateBack;
  flightsearchUrl += "&passengers=" + passengers;

  window.location = flightsearchUrl;
}

//Sets up the date pickers
function createDatePickers() {
  $("#flightOutDate").datepicker().datepicker("setDate", new Date());
  $("#flightOutDate").datepicker( "option", "minDate", 0 );
  $("#flightBackDate").datepicker().datepicker("setDate", new Date());
  $("#flightBackDate").datepicker( "option", "minDate", 0 );
}

function airportSearch(term) {
  $.ajax({
    type: "GET",
    url: airportUrl + term,
    contentType: "application/json",
    dataType: 'json',
    cache: false,
    success: function(data){
      airports = data;
    }
  })
}

$( function() {
  $( "#flightOutLocation" ).autocomplete({
    minLength: 0,
    source: function(request, response) {
      airportSearch(request.term);
      var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(airports, function(value) {
        return matcher.test(value.Country) || matcher.test(value.City_Town) || matcher.test(value.Airport_Name);
      }));
    },
    focus: function( event, ui ) {
      $( "#flightOutLocation" ).val( ui.item.Airport_Name );
      $( "#flightOutIATA" ).val( ui.item.Airport_Name );
      return false;
    },
    select: function( event, ui ) {
      $( "#flightOutLocation" ).val( ui.item.Airport_Name );
      $( "#flightOutIATA" ).val( ui.item.Airport_Name );
      return false;
    }
  })
  .autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( `
                <div>
                  <p> ${item.Airport_Name}  -   <span class="smallText"> ${item.City_Town}   -   ${item.Country}   -   ${item.IATA_Code} </span> </p>
                </div>`
              )
      .appendTo( ul );
  };
});

$( function() {
  $( "#flightBackLocation" ).autocomplete({
    minLength: 0,
    source: function(request, response) {
      airportSearch(request.term);
      var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
      response($.grep(airports, function(value) {
        return matcher.test(value.Country) || matcher.test(value.City_Town) || matcher.test(value.Airport_Name);
      }));
    },
    focus: function( event, ui ) {
      $( "#flightBackLocation" ).val( ui.item.Airport_Name );
      $( "#flightBackIATA" ).val( ui.item.Airport_Name );
      return false;
    },
    select: function( event, ui ) {
      $( "#flightBackLocation" ).val( ui.item.Airport_Name );
      $( "#flightBackIATA" ).val( ui.item.Airport_Name);
      return false;
    }
  })
  .autocomplete( "instance" )._renderItem = function( ul, item ) {
    return $( "<li>" )
      .append( `
                <div>
                  <p> ${item.Airport_Name}  -   <span class="smallText"> ${item.City_Town}   -   ${item.Country}   -   ${item.IATA_Code} </span> </p>
                </div>`
              )
      .appendTo( ul );
  };
});
