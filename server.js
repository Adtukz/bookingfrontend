const express = require('express');
const app = express();
const body_parser = require('body-parser');
const urlencoded_parser = body_parser.urlencoded({ extended: true });
app.use(express.static('public'));

app.get('/',(req,res)=>{
  res.sendFile(__dirname + '/pages/index.html');
});
app.get('/flightsearch/*',(req,res)=>{
  res.sendFile(__dirname + '/pages/flightsearch.html');
});
app.get('/flightconfirm/*',(req,res)=>{
  res.sendFile(__dirname + '/pages/flightconfirm.html');
});

app.listen(8088)
console.log("Listening on port 8088");

module.exports = app;
